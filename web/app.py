from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

# Found this method on this site:
# https://stackoverflow.com/questions/13678397/python-flask-default-route-possible

@app.route("/", defaults={'path': ''})
@app.route("/<path:path>")
def hello(path):
    fileDir = "templates/" + path

    if path.endswith(('.css', '.html')):
        if '//' in path or '..' in path or '~' in path:
            return error_403()
        elif path.startswith('//'):
            return error_403()
        else:
            return error_404()

def error_404():
    return render_template("404.html"), 404
def error_403():
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
